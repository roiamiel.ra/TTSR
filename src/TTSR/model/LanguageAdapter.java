package TTSR.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Abstract class to adapt specific language roles to the TTSR system
 *
 * OneLetter - Represents single letter object
 *
 * SoundObject - Object that hold a sound
 *
 * init function - needs to install your language adapter (things like voices list etc.)
 *
 * getSeparateVoices function - distribute a whole word to parts
 *                  (syllable / whole word / or just a single letter)
 *
 * @author ManWithAndroid (roiamiel.ra@gmail.com)
 * @version 1.0
 *
 * Created on 03/03/2017.
 */
public abstract class LanguageAdapter<OneLetter extends Letter, SoundObject> {

    /**
     * List to keep all the sounds in key - object, model
     * Key - the part (syllable / whole word / or just a single letter) in string
     * Object - The sound reference
     */
    private HashMap<String, SoundObject> mVoicesList;

    /**
     * Function to install the language adapter
     * Voices list, etc.
     */
    public abstract void init();

    /**
     * Function to distribute a whole word to parts
     * Each part can be a letter \ syllable \ or the whole word
     * But each part needs to represents a real sound that exists in the voices list.
     *
     * Example, the word "Hello":
     *          Word - { 'H', 'e', 'l', 'l', 'o' }
     *          Hello -> He, ll, o
     *                 = Hel, l, o (more accurate sounds)
     *
     *          each part is the index of his sound in the voices list
     *
     * The function will return this index's of sound,
     * that wil be connect to one world
     *
     * @param Word The word built form a OneLetter array
     * @return Return the index's of the sounds object
     */
    public abstract ArrayList<SoundObject> getSeparateVoices(OneLetter[] Word);

    /**
     * Function to clear the sounds list
     */
    protected void clearSoundList() {
        //Clear the sound list hash map
        this.mVoicesList.clear();
    }

    /**
     * Function to get the keys string array of the sounds list
     * @return the keys string array
     */
    protected String[] getSoundListKeys() {
        //Return the sounds list key as string array
        return this.mVoicesList.keySet()
                .toArray(
                        new String[this.mVoicesList.size()]
                );
    }

    /**
     * Function to get a specific sound from sounds list by key
     * @param Key The key of the sound
     * @return The specific object (null for error)
     */
    protected SoundObject getSoundByKey(String Key) {
        //Check key valid
        if(Key == null || Key.equals("")) {
            //Return error (null)
            return null;
        }

        //Return the specific sound from sound list (null if not exists)
        return this.mVoicesList.get(Key);
    }

    /**
     * Function to put sound to the sound list by key
     * @param Key The key to put the sound
     * @param Sound The sound to append
     * @return The success status (true or false)
     */
    protected boolean putSoundByKey(String Key, SoundObject Sound) {
        //Check key valid
        if(Key == null || Key.equals("")) {
            //Return error (false)
            return false;
        }

        //Put the sound
        this.mVoicesList.put(Key, Sound);

        //Return success
        return true;
    }
}
