package TTSR.model;

/**
 * Object to hold a single letter
 *
 * @author ManWithAndroid (roiamiel.ra@gmail.com)
 * @version 1.0
 *
 * Created on 06/03/2017.
 */
public abstract class Letter {

    /**
     * Function to convert the whole letter parts in to one key
     * @return The letter (with the additives) as text (string object)
     */
    public abstract String toString();

}
